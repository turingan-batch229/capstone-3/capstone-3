import {Row, Col, Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Highlights(){
	return(
		<Row className="m-3">
            <Col xs={12} md={4}>
                <Card id="card1" className="cardHighlight p-3">
                    <Card.Body>
                    
                        <Card.Title>
                            <h2>Cakes</h2>
                        </Card.Title>
                        <Card.Text>
                            Artisanal cakes customized to your liking for your preferred occassion.
                        </Card.Text>
                        <Link className= 'btn btn-primary' to ="/products">View Products</Link>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card id="card2" className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Pastries</h2>
                        </Card.Title>
                        <Card.Text>
                            From bread bites to a whole loaf, everything is guaranteed freshly baked everyday!
                        </Card.Text>
                        <Link className= 'btn btn-primary' to ="/products">View Products</Link>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card id="card3" className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Noodle dish</h2>
                        </Card.Title>
                        <Card.Text>
                            Coming soon! From pastas to ramen, every ingredients made from scratch.
                        </Card.Text>
                        <Link className= 'btn btn-primary' to ="/notfound" disabled>Coming Soon!</Link>
                    </Card.Body>
                </Card>
            </Col>
        </Row>


		)
}