import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react'
import {Navigate, Link, useNavigate} from 'react-router-dom'
import UserContext from '../UserContext.js'
import Swal from 'sweetalert2'

export default function New() {
	const navigate = useNavigate();

	const [name, setName] = useState();
	const [description, setDescription] = useState();
	const [price, setPrice] = useState();

	const [ isActive, setIsActive ] = useState(false);

	useEffect(() => {
		if(name !=='' && description !=='' && price !== ''){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	})


	const addProduct =(e) => {
		e.preventDefault();
		fetch(`${ process.env.REACT_APP_API_URL }/products/new`, {
			method: "POST",
			headers: {
				"Content-Type" :"application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title:"Added!",
					icon:"success",
					text:"You have successfully added a new delicacy"
				})

				setName('')
				setDescription('')
				setPrice('')
				navigate("/admin");
			} else {
				Swal.fire({
					title: "Oops!",
					icon:"error",
					text:"Please try again"
				})
			}
		})
	}

	return(

		<Form onSubmit = {(e) =>addProduct(e)}>
			<h1>Add Product</h1>
			<Form.Group className="mb-1" controlId="pName">
				<Form.Label>Product Name</Form.Label>
				<Form.Control 
				type ='text' 
				placeholder='Product name'
				value={name}
				onChange = {e => setName(e.target.value)}
				required/>
			</Form.Group>

			<Form.Group className="mb-1" controlId="pDesc">
				<Form.Label>Description</Form.Label>
				<Form.Control 
				type ='text' 
				placeholder='Description'
				value={description}
				onChange = {e => setDescription(e.target.value)}
				required/>
			</Form.Group>

			<Form.Group className="mb-1" controlId="pPrice">
				<Form.Label>Price</Form.Label>
				<Form.Control 
				type ='number' 
				placeholder=''
				value={price}
				onChange = {e => setPrice(e.target.value)} 
				required/>
			</Form.Group>
			{
				(isActive) ? 
				<Button variant="primary" type="submit" controlId="submitBtn">
			Add Product
				</Button>
				:
				<Button variant="primary" type="submit" controlId="submitBtn" disabled>
			Add Product

				</Button>
			}

			<Link className= 'btn btn-danger m-3'to='/admin'> Back </Link>

		</Form>
		)
}
