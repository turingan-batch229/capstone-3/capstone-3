import UserOrder from '../components/UserOrder.js'
import {Container, Card, Row, Col} from 'react-bootstrap'
import {useState, useEffect} from 'react'
import {Link} from 'react-router-dom'

export default function CustProf(){

	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [email, setEmail] = useState('')
	const [mobileNo, setMobileNo] = useState('')

	useEffect(() => {
		fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
			method: 'POST',
			headers: {
				"Content-Type" :"application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setFirstName(data.firstName)
			setLastName(data.lastName)
			setEmail(data.email)
			setMobileNo(data.mobileNo)
		})
	}, [])

	const [orders,setOrders] =useState([])

	useEffect(() => {
		fetch(`${ process.env.REACT_APP_API_URL }/orders/getOrders/`, {
			method: 'GET',
			headers: {
				"Content-Type" :"application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setOrders(data.map(order =>{
				return(
					<UserOrder key ={order._id} orderProps={order}/>
					)
			}).reverse())
	
		})
	}, [])

	return(

<Container className="mt-5 text-justify">
	<Row>
		<Col lg={{ span: 4, offset: 4 }}>
		<>
			<Card className ='mx-4 my-3 p-3'>
				<Card.Body>
					<Card.Title className="mb-0">Full Name</Card.Title>
					<Card.Text id="cardcust" className = "mt-0">{firstName} {lastName}</Card.Text>
					<Card.Subtitle>Email:</Card.Subtitle>
					<Card.Text>{email}</Card.Text>
					<Card.Subtitle>Contact Number:</Card.Subtitle>
					<Card.Text>+63-{mobileNo}</Card.Text>
				</Card.Body>
			</Card>
			<h5 className ='mx-4 text-center'>List of orders</h5>
			{orders}
		</>			
		</Col>
	</Row>
</Container>


		

		)
}
