import AdminDashboard from '../components/AdminDashboard.js'
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import {Navigate, Link, useNavigate} from 'react-router-dom'
import {useEffect, useState, useContext} from 'react'
import UserContext from '../UserContext.js';	

export default function Products(){
	const {user, setUser} = useContext(UserContext);

	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [email, setEmail] = useState('')
	const [mobileNo, setMobileNo] = useState('')

	useEffect(() => {
		fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
			method: 'POST',
			headers: {
				"Content-Type" :"application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setFirstName(data.firstName)
			setLastName(data.lastName)
			setEmail(data.email)
			setMobileNo(data.mobileNo)
		})
	}, [])

	const [products, setProducts] = useState([]);

	useEffect(() => {
		
		fetch(`${ process.env.REACT_APP_API_URL }/products/allItems`, {
			method: 'GET',
			headers: {
				"Content-Type" :"application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProducts(data.map(product =>{
				return(
					<AdminDashboard key={product._id} productProps={product}/>
					)
			}))
		})
	}, [])

	return(

		<Container className="mt-3 pt-2 text-center">
			<Row>
				<Col lg={{ span: 4, offset: 4 }}>
				
				<Card className ='mx-4 my-3 p-3 text-justify' >
				<Card.Body>
					<Card.Text className= 'mb-0'>Full Name: {firstName} {lastName}</Card.Text>
					<Card.Text className= 'mb-0'>Email: {email}</Card.Text>
					<Card.Text className= 'mb-0'>Mobile No: {mobileNo}</Card.Text>
				</Card.Body>
			</Card>
				<>
				
			<h1 className='text-center'>All Products</h1>
			{products}			
		</>
		
		<Link className= 'btn btn-primary btn-block' to ={`/addProduct`}>Add product</Link>
		
					
				</Col>
			</Row>
		</Container>

		)
}
