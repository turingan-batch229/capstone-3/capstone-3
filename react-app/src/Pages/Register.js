import {Form, Button, Container, Row, Col} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import {useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';


export default	function Register(){

	const navigate = useNavigate();

	const {user} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [fName, setFname] = useState('');
	const [lName, setLname] = useState('');
	const [add, setAdd] = useState('');
	const [mobNo, setMobNo] = useState('');

	const [isActive, setIsActive] = useState(false);


	useEffect(() =>{
		
		if((email !== '' && password1 !== '' && password2 !== '' && fName !== '' && lName !== ''  && add !== '') && (password1 === password2)){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	}, [email, password1, password2, fName, add, lName, mobNo])

	
	const registerUser = (e) => {
		e.preventDefault();
		fetch(`http://localhost:4020/users/checkEmail`, {
		method: "POST",
		headers: {
						"Content-Type": "application/json",
					},
		body: JSON.stringify({
			email: email
		})
	})
	.then(res => res.json())
	.then(data => {

		console.log(data);

		if(data === false){
			fetch(`http://localhost:4020/users/register`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				firstName: fName,
				lastName: lName,
				email: email,
				address: add,
				mobileNo: mobNo,
				password: password1,
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data === true){
				Swal.fire({
					title: "Successfully Registered",
					icon: 'success',
					text: `You have successfully registered ${email}.`
				})

				navigate("/login");

			}else{
				Swal.fire({
					title: "Something went wrong!",
					icon: 'error',
					text: "Please try again."
				})
			}

		});

	}else{
		Swal.fire({
					title: "Something went wrong!",
					icon: 'error',
					text: "Email already exists!"
				})
	}

})

	setEmail('');
	setPassword1('');
	setPassword2('');
	setFname('');
	setLname('');
	setAdd('');
	setMobNo('');

}	

	return(

		(user.id !== null) ?

		<Navigate to="/"/>
		:

<Container className="mt-5 text-justify">
	<Row>
		<Col lg={{span: 6}}>
		<>
			<Form onSubmit={(e) => registerUser(e)}>
		  <Form.Group className="mb-3" controlId="userEmail">
		    <Form.Label>Email address</Form.Label>
		    <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
		    <Form.Text className="text-muted">
		      We'll never share your email with anyone else.
		    </Form.Text>
		  </Form.Group>

		  <Form.Group className="mb-3" controlId="password1">
		    <Form.Label>Password</Form.Label>
		    <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)} required />
		  </Form.Group>

		   <Form.Group className="mb-3" controlId="password2">
		    <Form.Label>Verify Password</Form.Label>
		    <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
		  </Form.Group>


		   <Form.Group className="mb-3" controlId="firstName">
		    <Form.Label>First Name</Form.Label>
		    <Form.Control type="text" placeholder="First Name" value={fName} onChange={e => setFname(e.target.value)} required/>
		  </Form.Group>


		   <Form.Group className="mb-3" controlId="lastName">
		    <Form.Label>Last Name</Form.Label>
		    <Form.Control type="text" placeholder="Last Name" value={lName} onChange={e => setLname(e.target.value)} required/>
		  </Form.Group>

		     <Form.Group className="mb-3" controlId="address">
		    <Form.Label>Address</Form.Label>
		    <Form.Control type="text" placeholder="Address" value={add} onChange={e => setAdd(e.target.value)} required/>
		  </Form.Group>



		   <Form.Group className="mb-3" controlId="mobNo">
		    <Form.Label>Mobile No.</Form.Label>
		    <Form.Control type="number" maxLength={10} placeholder="Mobile No" value={mobNo} onChange={e => setMobNo(e.target.value)} required/>
		  </Form.Group>

	{
		(isActive) ? 
	  	<Button variant="primary" type="submit" controlId="submitBtn" to="/login">Register
	  	</Button>
	  	:
	  		<Button variant="primary" type="submit" controlId="submitBtn" disabled>
	  	Register
	  	</Button>
	  		}
	  	</Form>
		</>			
		</Col>
	</Row>
</Container>


		
		)
}

