import {useState, useEffect} from 'react';
import './App.css';
import AppNavbar from './components/AppNavbar.js';
import {Container} from 'react-bootstrap/';
import{BrowserRouter as Router, Route, Routes} from 'react-router-dom/';
import Home from './Pages/Home.js';
import Products from './Pages/Products.js';
import UpdateProduct from './Pages/UpdateProduct.js';
import NewProduct from './Pages/NewProduct.js';
import ProductsView from './components/ProductsView.js';
import Register from './Pages/Register.js';
import CustPage from './Pages/CustPage.js';
import AdminPage from './Pages/AdminPage.js'
import Login from './Pages/Login.js';
import Logout from './Pages/Logout.js';
import NotFound from './Pages/NotFound.js';
import {UserProvider} from './UserContext.js';


function App() {

  const [user, setUser] = useState({

/*    token: localStorage.getItem('token'),
    isAdmin:localStorage.getItem('isAdmin'),*/
    id: null,
    isAdmin: null
    
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  })

  useEffect(()=>{

    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      method: 'POST',
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

     
      if(data._id !== undefined){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      }
      
      else{
        setUser({
          id: null,
          isAdmin: null
        });
      }
      
    })

  }, [])


  return (
   
  <UserProvider value={{user, setUser, unsetUser}}>
   <> 
    <Router>
      <AppNavbar/>

      <Container id="container">
        <Routes>
            <Route exact path="/" element={<Home/>}/>
           
            
            <Route exact path="/productsView/:productId" element={<ProductsView/>}/>
            <Route exact path ="/admin" element={<AdminPage/>}/>
            <Route exact path="/login" element={<Login/>}/>
            <Route exact path="/products" element={<Products/>}/>
            <Route exact path ="/user/:userId" element={<CustPage/>}/>
            <Route exact path="/logout" element={<Logout/>}/>
            <Route exact path="/register" element={<Register/>}/>
            <Route exact path="/addProduct" element={<NewProduct/>}/>
            <Route exact path ="/:itemId/update" element={<UpdateProduct/>}/>
            <Route path='*' element={<NotFound/>}/>

        </Routes>
      </Container>        
    </Router>
  </>
  </UserProvider>
  );

}

export default App;
